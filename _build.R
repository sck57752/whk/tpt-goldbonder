# Build Book
purrr::map(
  .x = list.files(
    path = here::here("R"),
    full.names = TRUE
  ),
  .f = source
)

if (fs::dir_exists(here::here("output/report"))) {
  fs::dir_delete(here::here("output/report"))
}

bookdown::render_book(
  input = here::here("report"),
  output_format = "all",
  output_dir = here::here("output/report"),
  params = list(
    show_code = FALSE
  )
)

if (fs::dir_exists(here::here("output/report/download")) == FALSE) {
  fs::dir_create(here::here("output/report/download"))
}

fs::file_move(
  path = here::here("output/report/tpt-goldbonder.pdf"),
  new_path = here::here("output/report/download/tpt-goldbonder.pdf")
)

fs::file_move(
  path = here::here("report/tpt-goldbonder.html"),
  new_path = here::here("output/report/download/tpt-goldbonder.html")
)
